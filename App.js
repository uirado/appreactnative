import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Main from './app/pages/main';

export default class App extends Component {
  render() {
    return (
      <View style={styles.app}>
        <Main />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    backgroundColor: '#FFF',
  },
});
