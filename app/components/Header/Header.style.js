import { StyleSheet } from 'react-native';

const s = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  topBar: {
    flexDirection: 'row',
    padding: 5,
    backgroundColor: '#5555ee',
  },
  title: {
    flex: 1,
    paddingRight: 40,
    marginTop: 2,
    lineHeight: 38,
    fontSize: 24,
    height: 40,
    fontWeight: 'normal',
    color: '#FFF',
    fontFamily: 'Playball-Regular',
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    height: 40,
    width: 30,
    marginRight: 10,
  },
  actionIcon: {
    flex: 1,
    fontSize: 30,
    color: '#FFF',
    textAlign: 'center',
  },
  tabs: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  tabButton: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#5555ee',
    paddingTop: 5,
    paddingBottom: 5,
    alignItems: 'center',
  },
  tabIcon: {
    flex: 1,
    color: '#FFF',
    fontSize: 18,
  },
  buttonTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
    color: '#FFF',
    fontSize: 14,
  },
});

export default s;
