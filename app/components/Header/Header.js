import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-fa-icons';
import s from './Header.style';

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = { titulo: this.props.titulo };
  }

  render() {
    return (
      <View style={s.container}>
        <View style={s.topBar}>
          <TouchableOpacity
            style={s.actionButton}
            activeOpacity={0.7}
            onPress={() => {
              Alert.alert('Esse botão não faz nada!');
            }}
          >
            <Text style={s.actionIcon}>
              <Icon name="anchor" allowFontScaling />
            </Text>
          </TouchableOpacity>
          <Text style={[s.title, { textAlign: this.props.align }]}>
            {this.state.titulo}
          </Text>
        </View>
        <View style={s.tabs}>
          <TouchableOpacity
            style={s.tabButton}
            activeOpacity={0.7}
            onPress={() => {
              this.setState({ titulo: 'Pedra' });
            }}
          >
            <Icon name="hand-rock-o" style={s.tabIcon} />
            <Text style={s.buttonTitle}>PEDRA</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={s.tabButton}
            activeOpacity={0.7}
            onPress={() => {
              this.setState({ titulo: 'Papel' });
            }}
          >
            <Icon name="hand-paper-o" style={s.tabIcon} />
            <Text style={s.buttonTitle}>PAPEL</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[s.tabButton, { marginRight: 0 }]}
            activeOpacity={0.7}
            onPress={() => {
              this.setState({ titulo: 'Tesoura' });
            }}
          >
            <Icon name="hand-scissors-o" style={s.tabIcon} />
            <Text style={s.buttonTitle}>TESOURA</Text>
          </TouchableOpacity>
        </View>
        <Text>{this.state.titulo}</Text>
      </View>
    );
  }
}

Header.propTypes = {
  titulo: PropTypes.string,
  align: PropTypes.string,
};

Header.defaultProps = {
  titulo: 'Titulo do Header',
  align: 'left',
};
